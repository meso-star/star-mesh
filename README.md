# Star-Mesh

Star-Mesh loads surfacic or volumetric meshes saved in Star-Mesh file
format. See smsh.5 for details.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1

- Make memory mapping optional. By default, data is now loaded into
  memory. Memory mapping becomes an option of the load functions,
  (forbidden on stdin). As a consequence, this commit introduces API
  breaks.
- Write the man page directly in mdoc's roff macros, instead of using
  the intermediate scdoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

## License

Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)

Star-Mesh is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
